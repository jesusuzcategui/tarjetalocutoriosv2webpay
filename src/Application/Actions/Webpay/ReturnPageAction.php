<?php

declare(strict_types=1);

namespace App\Application\Actions\Webpay;

use Exception;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;
use \PDO;

class ReturnPageAction extends WebpayAction
{
    /**
     * {@inheritdoc}
     */

    public function __construct(LoggerInterface $logger) {
        parent::__construct($logger);
        WebpayPlus::configureForTesting();
    }

    protected function action(): Response
    {
        try {

            $queryParams = (array) $this->request->getQueryParams();
            $transaction = (new Transaction)->commit($queryParams["token_ws"]);
            
            if( $transaction->responseCode == 0 ){
                $this->logger->info("La compra con ID {$transaction->buyOrder} realizada el {$transaction->transactionDate} tuvo una respuesta: {$transaction->responseCode} y un estatus {$transaction->status} y su VCI fue {$transaction->vci}");
                $queryPin = <<<SQL
                SELECT tar.id, tar.pin, mon.monto FROM ventas_frecuentes AS ven LEFT JOIN targetas AS tar ON tar.id = ven.id_targeta LEFT JOIN monto AS mon ON mon.id = tar.precio WHERE ven.id_operacion = '{$transaction->buyOrder}'
                SQL;
                $resultPin = $this->database->query($queryPin)->fetch(PDO::FETCH_OBJ);
                
                if(is_bool($resultPin)){
                    $this->logger->info("Error en base de datos para trasaccion: {$transaction->buyOrder} {json_encode($this->database->error)}");
                    return $this->response->withHeader('Location', 'http://localhost:8080/validate')->withStatus(301);
                }                
                
                $updateSale = $this->database->update('ventas_frecuentes', ['estado' => 3, 'mensaje_webpay' => json_encode($transaction), 'fin' => date("Y-m-d H:i:s", time() - 3600)], ['id_operacion' => $transaction->buyOrder]);
                $updateCard = $this->database->update('targetas', ['estado_id' => 3], ['id' => $resultPin->id]);
                
                
                
                if($updateSale->rowCount() == 0 || $updateCard->rowCount() == 0){
                    $errorDb = json_encode($this->database->error);
                    $this->logger->info("Error en base de datos para trasaccion: {$transaction->buyOrder} {$errorDb}");
                    return $this->response->withHeader('Location', 'http://localhost:8080/validate')->withStatus(301);
                }
                
                return $this->respondWithData($resultPin);
                
                /*$this->logger->info("Venta y tarjeta actualizadas");
                
                $this->response->withHeader('Location', 'http://localhost:8080/validate')->withStatus(301);*/
                
            }

        } catch(Exception $e) {
            return $this->respondWithData([
                "msg" => $e->getMessage(),
                "code" => $e->getCode()
            ])->withStatus(400);
        }
    }
}
