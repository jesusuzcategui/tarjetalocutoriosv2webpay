<?php

declare(strict_types=1);

namespace App\Application\Actions\Webpay;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;
use Psr\Log\LoggerInterface;
use \PDO;

class InitTransactionAction extends WebpayAction
{
    /**
     * {@inheritdoc}
     */

    public function __construct(LoggerInterface $logger) {
        parent::__construct($logger);
        WebpayPlus::configureForTesting();
    }

    protected function action(): Response
    {
        try {

            //$transaction = (new Transaction)->create('0001', 'lcbuy_0001', 5000, 'https://tarjetalocutoriosv2webpay.test/webpay/returnPage');
            if((empty(session_id()) == true)){
                session_start();
            }
            
            $paymentData = (object) $this->request->getParsedBody();
            $typeData = gettype($paymentData);
            $paymentData->buyOrder = strval( rand(100000, 999999999) );
            $paymentData->sessionId = session_id();
            
            $queryIfCard = <<<SQL
            SELECT id, cod_targ, pin FROM targetas WHERE precio = {$paymentData->ammoutCode} AND estado_id = 1 LIMIT 1
            SQL;
            
            $verifyExistence = $this->database->query($queryIfCard);
            
            if(is_null($verifyExistence)){
                $error = json_encode($this->database->errorInfo);
                $this->logger->error("Error de MYSQL al consultas tarjetas: {$error}");
                return $this->respondWithData( $this->database->errorInfo, 406 );
            }
            
            $cardForSale = $verifyExistence->fetch(PDO::FETCH_OBJ);
            
            if(is_bool($cardForSale)){
                $this->logger->info("No hay tarjetas de {$paymentData->ammout}");
                return $this->respondWithData( ["message" => "No hay tarjetas disponibles"], 405 );
            }
            
            $dateSale = date("Y-m-d H:i:s", time() - 3600);
            
            $this->database->insert('ventas_frecuentes', [
                "id_targeta" => $cardForSale->id,
                "id_estatus" => 1,
                "fecha" => $dateSale,
                "id_operacion" => $paymentData->buyOrder,
                "id_usu" => 1,
                "correo" => strtolower($paymentData->email),
                "correo_cliente" => strtolower($paymentData->email),
                "estado" => 2,
                "telefono" => intval($paymentData->phone),
                "tipo_venta" => "N/A",
                "mensaje_webpay" => "N/A",
                "inicio" => $dateSale
            ]);
            
            $idSale = $this->database->id();
            
            if(empty($idSale)){
                $this->logger->info("Error al registrar venta en base de datos");
                return $this->respondWithData( ["message" => "Error de servidor"], 500 );
            }
            
            $updateCard = $this->database->update('targetas', ["estado_id" => 2], ["id" => $cardForSale->id]);
            
            if($this->database->error || $updateCard->rowCount() == 0){
                $this->logger->info("Error al actualizar tarjeta con id {$cardForSale->id}");
                return $this->respondWithData( ["message" => "Error de servidor"], 500 );
            }
            
               
            $this->logger->notice("Comenzamos transacción con id {$idSale} y tarjeta id {$cardForSale->id}");
            
            $transaction = (new Transaction)->create($paymentData->buyOrder, $paymentData->sessionId, $paymentData->ammout, 'https://tarjetalocutoriosv2webpay.test/webpay/returnPage');
            
            return $this->respondWithData(["webpay" => $transaction, "card" => $cardForSale->id, "sale" => $idSale]);

        } catch(Exception $e) {
            return $this->respondWithData([
                "msg" => $e->getMessage(),
                "code" => $e->getCode()
            ])->withStatus(400);
        }

        return $this->respondWithData(["Error"]);
    }
}
