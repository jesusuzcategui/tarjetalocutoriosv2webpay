<?php

declare(strict_types=1);

namespace App\Application\Actions\Webpay;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;

class CreatePaymentAction extends WebpayAction
{
    /**
     * {@inheritdoc}
     */

    public function __construct() {
        WebpayPlus::configureForTesting();
    }

    protected function action(): Response
    {
        try {

            $transaction = (new Transaction)->create('0001', 'lcbuy_0001', 5000, 'https://tarjetalocutoriosv2webpay.test/webpay/returnPage');
            return $this->respondWithData($transaction);

        } catch(Exception $e) {
            return $this->respondWithData([
                "msg" => $e->getMessage(),
                "code" => $e->getCode()
            ])->withStatus(400);
        }

        return $this->respondWithData(["Error"]);
    }
}
