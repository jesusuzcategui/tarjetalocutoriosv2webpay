<?php

declare(strict_types=1);

namespace App\Application\Actions\Tarjetas;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Transbank\Webpay\WebpayPlus;
use Transbank\Webpay\WebpayPlus\Transaction;

class GetAviableTarjetasAction extends TarjetasAction
{
    /**
     * {@inheritdoc}
     */

    public function __construct() {
        WebpayPlus::configureForTesting();
    }

    protected function action(): Response
    {
        try {
            $query = <<<SQL
            SELECT CAST(m.monto AS DECIMAL) monto, COUNT(m.id) AS cant, m.estatus_id AS estado, m.id idprecio
            FROM monto m
            LEFT JOIN targetas t ON t.precio = m.id
            GROUP BY m.id
            HAVING cant > 0 AND estado = 1
            SQL;
            
            $data = $this->database->query($query)->fetchAll(\PDO::FETCH_OBJ);
            
            $response = array_map(function($precio){
                $element = clone $precio;
                $element->monto = (int) $element->monto;
                $element->cant = (int) $element->cant;
                $element->estado = (int) $element->estado;
                $element->idprecio = (int) $element->idprecio;
                return $element;
            }, $data);
            
            return $this->respondWithData($response);
        } catch (Exception $e) {
            return $this->respondWithData(["message" => $e->getMessage(), "code" => $e->getCode()], 400);
        }
    }
}
