<?php

declare(strict_types=1);

namespace App\Application\Actions\Tarjetas;

use App\Application\Actions\Action;
use Psr\Log\LoggerInterface;

abstract class TarjetasAction extends Action
{
    
    public function __construct(LoggerInterface $logger){
        parent::__construct($logger);
    }
}
